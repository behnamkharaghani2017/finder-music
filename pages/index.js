import React ,{useEffect,useState} from 'react'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
// import '../styles/style.css'
import axios from 'axios'
// import { useState } from 'react/cjs/react.production.min'

export default function Home() {
  const [tracks,setTracks] = useState({});
  useEffect(() =>{
    const body = document.body;
const bgColorsBody = ["#ffb457", "#ff96bd", "#9999fb", "#ffe797", "#cffff1"];
const menu = body.querySelector(".menu");
const menuItems = menu.querySelectorAll(".menu__item");
const menuBorder = menu.querySelector(".menu__border");
let activeItem = menu.querySelector(".active");

function clickItem(item, index) {

    menu.style.removeProperty("--timeOut");
    
    if (activeItem == item) return;
    
    if (activeItem) {
        activeItem.classList.remove("active");
    }

    
    item.classList.add("active");
    body.style.backgroundColor = bgColorsBody[index];
    activeItem = item;
    offsetMenuBorder(activeItem, menuBorder);
    
    
}

function offsetMenuBorder(element, menuBorder) {

    const offsetActiveItem = element.getBoundingClientRect();
    const left = Math.floor(offsetActiveItem.left - menu.offsetLeft - (menuBorder.offsetWidth  - offsetActiveItem.width) / 2) +  "px";
    menuBorder.style.transform = `translate3d(${left}, 0 , 0)`;

}

offsetMenuBorder(activeItem, menuBorder);

menuItems.forEach((item, index) => {

    item.addEventListener("click", () => clickItem(item, index));
    
})

window.addEventListener("resize", () => {
    offsetMenuBorder(activeItem, menuBorder);
    menu.style.setProperty("--timeOut", "none");
});
  },[])


const sendLink = (e)=>{
  e.preventDefault();
  let url = e.target[0].value;
  document.getElementById('alerty').innerText = '...در حال دریافت ویدیو از اینستاگرام'
  axios.post(`http://localhost:3000/api/insta/`, { url })
  .then(res => {
    console.log(res);
    if(res.status == '200'){
      document.getElementById('alerty').innerText = '...درحال جستجوی آهنگ'
      // res.data.voice_url
      console.log(res.data.voice_url)
      axios.post(`http://localhost:3000/api/shazam/`, { url :res.data.voice_url})
      .then(res1 => {
        console.log(res1);
        if(res1.status == '200'){
          setTracks({status:200,msg:'آهنگ مورد نظر یافت شد.',data:res1.data})
          document.getElementById('alerty').innerText = ''
        }else{
          setTracks({status:404,msg:res1.data.msg,data:''})
          document.getElementById('alerty').innerText = 'آهنگ مورد نظر یافت نشد.'
        }
      })
    }

  })
  // console.log(e.target[0].value)
}
const download_link = (e)=> {
  let id_track = e.currentTarget.getAttribute('data-id');
  console.log(id_track)
  document.getElementById('alerty').innerText = 'در حال دانلود چند لحظه صبرکنید...'
  axios.post(`http://localhost:3000/api/download-spotify/`, { id :id_track})
  .then(res1 => {
    console.log(res1);
    saveAs(res1.data.url);
    function saveAs(url) {    
      var filename = url.substring(url.lastIndexOf("/") + 1).split("?")[0];
      var xhr = new XMLHttpRequest();
      xhr.responseType = 'blob';
      xhr.onload = function() {
        var a = document.createElement('a');
        a.href = window.URL.createObjectURL(xhr.response);
        a.download = filename; 
        a.style.display = 'none';
        document.body.appendChild(a);
        a.click();
    
      };
      xhr.open('GET', url);
      xhr.send();
    }
    document.getElementById('alerty').innerText = ''
  })
}
  return (
<>
<div className='videoInsta'>
<div>
<form id='sendLink' onSubmit={sendLink}>
  <input type='text' />
  <button type='submit'>دریافت</button>
</form>
</div>
<div className='downloaddy'>
  {tracks.status=='200'&& tracks.data.map(el=><a key={el.id} data-id={el.id} onClick={(e)=>download_link(e)}>دانلود آهنگ</a>)}
</div>
<div id='alerty'>
</div>


    {/* <video width="400" controls>
      <source src="https://scontent.cdninstagram.com/v/t50.2886-16/280178769_1861464097376711_837855429104775681_n.mp4" type="video/webm" />
      <source src="https://scontent.cdninstagram.com/v/t50.2886-16/280178769_1861464097376711_837855429104775681_n.mp4" type="video/ogg" />
      Your browser does not support HTML5 video.
    </video>
    <video width="400" controls>
      <source src="https://cdn.botdef.com/img/sZD7vWbOSn7BAL7HuHGpfJu8faFaZYSBxMQEfqcwG5If-HKxpntfTBQ9EnuskE7GsIeRmUqhf-lBg1ZtMj8uTqebsBiaeWDkn5uToYkuD6dcb9PyjUXljm3zyeUUTBsBwB2OPTr53jwVDeC31RL3sK30EV6i9FIAOVUTuldvO9_BfaOmhCQV8weJ4T8msQnwfLoAxj-KUwbn_KrEOxE4hmvqpflbB5b2B3HRO5JXV0sr4qciagP-LT1wJIPu8VA5oUdDwg0yRZx7p6bKZwEg3_JkQE-iFGUm_5gbIb9Ud4E-YqR8oWyN_jONv0p9cUZDOI8g6u1580l1UYwwVhyvairb6RAiJggjAJXDJsFhHKeocRJUhiXKDDRD6KfV8IeJIrqTFdfjIRGx_W3i7ZMLCjpcnOPEPb1Nnxk6xnQvfP_RgGBsXbsmuwR0cfL2-ftIJYAA8kddMNXFSa_7SUYFIO1wiVqvlgXWkEaR5fVkxZ2dXOs5vyk5jFKZbfHp2a3OI1ZsWldnj68cNSBhKygwSWNWubPsSoNngFoiICDEXo11XjTBV1Pisnp-MqGGzwfS1BM17W0scO7XQ0eziXlPwWRzW6iuwsjvohobG3rMFYDCEZ0nj5pQ5HunYv62ZHHXkgvqMYRlDmcbBTTXTMq_auTt3Q2Uv1dWD7gSBgGk-taZT8e88DmexNr4i1JB3KbfXaMxCeMDaYIOYuSqAoi5Lg" type="video/webm" />
      <source src="mov_bbb.ogg" type="video/webm" />
      Your browser does not support HTML5 video.
    </video> */}
</div>
 <div className='menu_wraper'>
 <menu className="menu">
    <button className="menu__item menu_item1 active">
      <svg className="icon" viewBox="0 0 24 24">
        <path d="M3.8,6.6h16.4" />
        <path d="M20.2,12.1H3.8" />
        <path d="M3.8,17.5h16.4" />
      </svg>
    </button>
    <button className="menu__item menu_item2">
      <svg className="icon" viewBox="0 0 24 24">
        <path
          d="M6.7,4.8h10.7c0.3,0,0.6,0.2,0.7,0.5l2.8,7.3c0,0.1,0,0.2,0,0.3v5.6c0,0.4-0.4,0.8-0.8,0.8H3.8
  C3.4,19.3,3,19,3,18.5v-5.6c0-0.1,0-0.2,0.1-0.3L6,5.3C6.1,5,6.4,4.8,6.7,4.8z"
        />
        <path d="M3.4,12.9H8l1.6,2.8h4.9l1.5-2.8h4.6" />
      </svg>
    </button>
    <button className="menu__item menu_item3">
      <svg className="icon" viewBox="0 0 24 24">
        <path d="M3.4,11.9l8.8,4.4l8.4-4.4" />
        <path d="M3.4,16.2l8.8,4.5l8.4-4.5" />
        <path d="M3.7,7.8l8.6-4.5l8,4.5l-8,4.3L3.7,7.8z" />
      </svg>
    </button>
    <button className="menu__item menu_item4">
      <svg className="icon" viewBox="0 0 24 24">
        <path
          d="M5.1,3.9h13.9c0.6,0,1.2,0.5,1.2,1.2v13.9c0,0.6-0.5,1.2-1.2,1.2H5.1c-0.6,0-1.2-0.5-1.2-1.2V5.1
    C3.9,4.4,4.4,3.9,5.1,3.9z"
        />
        <path d="M4.2,9.3h15.6" />
        <path d="M9.1,9.5v10.3" />
      </svg>
    </button>
    <button className="menu__item menu_item5">
      <svg className="icon" viewBox="0 0 24 24">
        <path
          d="M5.1,3.9h13.9c0.6,0,1.2,0.5,1.2,1.2v13.9c0,0.6-0.5,1.2-1.2,1.2H5.1c-0.6,0-1.2-0.5-1.2-1.2V5.1
    C3.9,4.4,4.4,3.9,5.1,3.9z"
        />
        <path d="M5.5,20l9.9-9.9l4.7,4.7" />
        <path d="M10.4,8.8c0,0.9-0.7,1.6-1.6,1.6c-0.9,0-1.6-0.7-1.6-1.6C7.3,8,8,7.3,8.9,7.3C9.7,7.3,10.4,8,10.4,8.8z" />
      </svg>
    </button>
    <div className="menu__border" />
  </menu>
  <div className="svg-container">
    <svg viewBox="0 0 202.9 45.5">
      <clipPath
        id="menu"
        clipPathUnits="objectBoundingBox"
        transform="scale(0.0049285362247413 0.021978021978022)"
      >
        <path
          d="M6.7,45.5c5.7,0.1,14.1-0.4,23.3-4c5.7-2.3,9.9-5,18.1-10.5c10.7-7.1,11.8-9.2,20.6-14.3c5-2.9,9.2-5.2,15.2-7
    c7.1-2.1,13.3-2.3,17.6-2.1c4.2-0.2,10.5,0.1,17.6,2.1c6.1,1.8,10.2,4.1,15.2,7c8.8,5,9.9,7.1,20.6,14.3c8.3,5.5,12.4,8.2,18.1,10.5
    c9.2,3.6,17.6,4.2,23.3,4H6.7z"
        />
      </clipPath>
    </svg>
  </div>
 </div>
  <style>
    
  </style>
</>

  )
}
