const path = require('path');
const axios = require('axios')
const ffmpeg = require('fluent-ffmpeg')
const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
const ffprobePath = require('@ffprobe-installer/ffprobe').path;

ffmpeg.setFfmpegPath(ffmpegPath);
ffmpeg.setFfprobePath(ffprobePath);

export default function handler(req, res) {
    if (req.method === 'POST') {
          // Process a POST request
        let url = req.body.url;
        const options = {
          method: 'GET',
          url: 'https://instagram-downloader-download-instagram-videos-stories.p.rapidapi.com/index',
          params: {url},
          headers: {
              'X-RapidAPI-Host': 'instagram-downloader-download-instagram-videos-stories.p.rapidapi.com',
              'X-RapidAPI-Key': 'bb45da607bmsha7101fcc1f83accp1b1a34jsn81bb41b54a2b'
          }
          };
      
          axios.request(options).then(async function (response) {
              // console.log(response.status );
              // res.send(response)
              
              if(response.status == '200'){
                let insta_url = response.data.media;
                const currentTimeInMilliseconds=Date.now();
                await ffmpeg()
                  .input(insta_url)
                  .save('public/'+currentTimeInMilliseconds+'.mp3');
                  res.status(200).json({voice_url:req.headers.host+'/'+currentTimeInMilliseconds+'.mp3'})
              }
              
      
              // let media = response.data.media;
          }).catch(function (error) {
              console.error(error);
          });
        // res.status(200).json({ url: req.body.url })
      } else {
        // Handle any other HTTP method
        console.log(req.query.url)
        
        // res.status(200).json({ method: req.query.url})
      }
    // res.json(req)

  }