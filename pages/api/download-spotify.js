const axios = require('axios')
var os = require('os'); // add at top
const http = require('http'); // or 'https' for https:// URLs
var https = require('https');
const fs = require('fs');

export default function handler(req, res) {
    if (req.method === 'POST') {
          // Process a POST request
        let id = req.body.id;
        const file = fs.createWriteStream(`public/${id}.mp3`);
        const request = https.get(`https://one-api.ir/spotify/?token=467120:626247e5b21af4.17865798&action=get_track&id=${id}`, async function(response) {
        await response.pipe(file);

        // after download completed close filestream
        await file.on("finish", () => {
            file.close();
            res.status(200).json({url:`http://${req.headers.host}/${id}.mp3`})

            console.log("Download Completed");
        });
        });

  }
}